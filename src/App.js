import React from 'react';
import CameraLoading from './component/CameraLoading';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CameraLoading/>
      </header>
    </div>
  );
}

export default App;
