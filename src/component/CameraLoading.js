import React, { useRef, useEffect } from 'react';
import { TimelineMax, Sine } from "gsap";
import '../style/camera-loading.scss';

const CameraLoading = () => {
    const tw = new TimelineMax();
    const stick1 = useRef();
    const stick2 = useRef();
    const stick3 = useRef();
    const stick4 = useRef();
    const stick5 = useRef();
    const stick6 = useRef();
    const stick7 = useRef();
    const stick8 = useRef();
    const camCircle = useRef();
    const logo = useRef();

    const disp = (name) => {
        const nm = document.getElementById(name);
        console.log(nm);
        nm.style.display = "block";
    }

    useEffect(() => {
        tw.to(stick1.current ,.25, {rotation:0, y:"+=64",x:"-=18", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.to(stick2.current ,.25, {rotation:45, y:"+=23", x:"-=63", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.to(stick3.current ,.25, {rotation:90, y:"-=15", x:"-=63", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.to(stick4.current ,.25, {rotation:135, y:"-=63", x:"-=21", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.to(stick5.current ,.25, {rotation:-180, y:"-=63", x:"+=25", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.to(stick6.current ,.25, {rotation:-135, y:"-=25", x:"+=63", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.to(stick7.current ,.25, {rotation:-90, y:"+=26", x:"+=63", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.to(stick8.current ,.25, {rotation:-45, y:"+=63", x:"+=26", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.to(camCircle.current ,.25, {rotation:170, repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1+=.5");
        tw.from(logo.current ,0.5, {opacity:0, onStart:disp, onStartParams:['logo'], ease:Sine.easeInOut},"kotak2");
    }, [tw]);

    return (
        <div className="camera-container">
            <div className="cam-cont">
                <div className="cam-circle" ref={camCircle}>
                    <div className="triangle-cont">
                        <div className="logo" id="logo" ref={logo}>TW</div>
                        <div className="triangle stick-1" ref={stick1}></div>
                        <div className="triangle stick-2" ref={stick2}></div>
                        <div className="triangle stick-3" ref={stick3}></div>
                        <div className="triangle stick-4" ref={stick4}></div>
                        <div className="triangle stick-5" ref={stick5}></div>
                        <div className="triangle stick-6" ref={stick6}></div>
                        <div className="triangle stick-7" ref={stick7}></div>
                        <div className="triangle stick-8" ref={stick8}></div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CameraLoading;
